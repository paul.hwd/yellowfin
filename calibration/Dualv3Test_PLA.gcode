; This gcode is formulated to work with dual tool head test stands running TAZ 6 Dual v2 firmware version 1.1.9.9
; Use of this gcode outside of an identical setup is likely to provide unexpected results
T0            ; Select Extruder 1
M104 S205     ; Begin Heating to 205
T1            ; Select Extruder 2
M104 S205     ; Begin heating to 205
M999          ; clear errors
M106 P1 S0    ; Cooling Fan Off
M106 P0 S0    ; Cooling Fan Off

M117 FANS 100 Percent
M106 P1 S255  ; turn fan on full speed
G4 S30        ; wait 30 seconds
M117 FAN OFF
M106 P1 S0    ; turn fan off

M400          ; flush buffer
G4 S5         ; wait 5 seconds
M117 FAN 50 PERCENT
M106 P1 S127  ; turn fan on at 50
G4 S10        ; wait 10 seconds
M117 FAN OFF
M106 P1 S0    ; turn fan off

M400          ; flush buffer
G4 S5         ; wait 5 seconds
M117 FAN 30 PERCENT
M106 P1 S77   ; turn fan on at 30 percent speed
G4 S10        ; wait 10 seconds
M117 FAN OFF
M106 P1 S0    ; turn fan off

M400          ; flush buffer
G4 S5         ; wait 5 seconds
M117 FAN ON LOW
M106 P1 S35   ; turn fan on at lowest setting
G4 S10        ; wait 10 seconds
M117 FAN OFF
M106 P1 S0    ; turn fan off

M117 PUSH X MAX SWITCH
M226 P24 S1

M999            ; clear errors
G21             ; set units to millimeters
G90             ; use absolute coordinates
M82             ; use absolute distances for extrusion
T1              ; use 2nd extruder
G92 E0          ; Set cords to zero
T0              ; activate first extruder
G92 E0          ; set E to 0

M109 R205       ; set first extruder nozzle to 205C and wait for it to reach temp
G1 E100 F100    ; move extruder 1 100mm
G1 E85 F100     ; move back 15mm 
M109 R205       ; wait before detemping

T1              ; change extruder
M109 R205       ; set second extruder nozzle to 205C and wait
G92 E0          ; set E to 0
G1 E100 F100    ; move extruder 2 100mm 
G1 E85 F100     ; move back 15mm
M109 S205       ; wait for extrusion before detempening

M117 Cooldown...
M106 P1 S255    ; Fan on 100 percent
M104 S0         ; Turn off 2nd extruder
T0              ; switch to 1st extruder
M109 R60        ; wait for temp
M106 P1 S0	; turn fan off
M84             ; turn off motors
